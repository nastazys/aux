package com.vantage.app.ui.slots;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vantage.app.R;
import com.vantage.app.model.LotItem;
import com.vantage.app.model.Seller;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class LotView extends RecyclerView.ViewHolder {

    private static DecimalFormat df = new DecimalFormat("#.##");
    private final LotsAdapter.OnLotClickListener listener;

    @BindView(R.id.lot_image)
    ImageView lotImage;

    @BindView(R.id.title_tv)
    TextView titleTV;

    @BindView(R.id.description_tv)
    TextView descriptionTV;

    @BindView(R.id.seller_avatar)
    CircleImageView sellerImage;

    @BindView(R.id.seller_tv)
    TextView sellerTV;

    @BindView(R.id.prive_tv)
    TextView priceTV;

    @BindView(R.id.card_view)
    CardView cardView;

    public LotView(@NonNull View itemView, LotsAdapter.OnLotClickListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
    }

    public void bind(final LotItem lotItem) {
        titleTV.setText(lotItem.getTitle());
        descriptionTV.setText(lotItem.getDescription());
        setImage(lotItem);
        setSeller(lotItem.getSeller());
        priceTV.setText(String.format("$%s", df.format(lotItem.getPrice())));
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLotClicked(lotItem);
            }
        });
    }

    private void setSeller(Seller seller) {
        if(TextUtils.isEmpty(seller.getAvatar())){
            // TODO: 2020-02-25 placeholder
        }else{
            Picasso.get().load(seller.getAvatar()).into(sellerImage, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d("sdfsdf", "onSuccess");
                }

                @Override
                public void onError(Exception e) {
                    Log.d("sdfsdf", "onError");
                }
            });
        }
        sellerTV.setText(String.format("%s(%s)", seller.getFullName(), df.format(seller.getRating())));
    }

    private void setImage(LotItem lotItem) {
        if(TextUtils.isEmpty(lotItem.getImage())){
            // TODO: 2020-02-25 placeholder
        }else{
            Picasso.get().load(lotItem.getImage()).into(lotImage);
        }
    }
}
