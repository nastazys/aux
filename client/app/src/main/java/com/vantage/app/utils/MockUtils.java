package com.vantage.app.utils;

import com.vantage.app.model.LotItem;
import com.vantage.app.model.Seller;

import java.util.ArrayList;

public class MockUtils {

    public static ArrayList<LotItem> getMockItems() {
        ArrayList<LotItem> list = new ArrayList<>();
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        list.add(getMockLotItem());
        return list;
    }

    public static LotItem getMockLotItem() {
        LotItem item = new LotItem();
        item.setTitle("NIKE Todos Panelled");
        item.setDescription("Powerful, strong, durable – every athlete and their clothes. With innovative sportswear designed to bring optimum comfort and support, Nike brings to you the best you’ll ever get. Shop for Nike sports shoes , clothing and accessories now with AJIO!");
        item.setPrice(99.3);
        item.setImage("https://imageup.ru/img211/3561645/item_image_placeholder.jpg");
        Seller seller = new Seller();
        seller.setAvatar("https://i.pravatar.cc/300\n");
        seller.setFullName("Michael P.");
        seller.setRating(4.3);
        item.setSeller(seller);
        return item;
    }
}
