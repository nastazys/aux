package com.vantage.app.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import com.vantage.app.R;
import com.vantage.app.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class LotDetailActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar.setTitle(R.string.lot_detail_title);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public static void show(Context context) {
        context.startActivity(new Intent(context, LotDetailActivity.class));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_lot_detail;
    }

    @OnClick(R.id.buy_btn)
    public void onButBtnClicked(){
        // TODO: 2020-02-25
    }

    @OnClick(R.id.minus_btn)
    public void onMinusBtnClicked(){
        // TODO: 2020-02-25
    }

    @OnClick(R.id.plus_btn)
    public void onPlusBtnClicked(){
        // TODO: 2020-02-25
    }

    @Override
    public boolean onNavigateUp() {
        onBackPressed();
        return super.onNavigateUp();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
