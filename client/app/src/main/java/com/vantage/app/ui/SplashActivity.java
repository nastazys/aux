package com.vantage.app.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.vantage.app.ui.slots.LotsListActivity;
import com.vantage.app.utils.Preferences;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showFirstScreen();
    }

    private void showFirstScreen() {
        if(Preferences.isUserAuthorized()){
            LotsListActivity.show(this);
        }else{
            SignInActivity.show(this);
        }
        finish();
    }
}
