package com.vantage.app.ui.slots;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.vantage.app.R;
import com.vantage.app.model.LotItem;
import com.vantage.app.ui.LotDetailActivity;
import com.vantage.app.ui.base.BaseActivity;
import com.vantage.app.utils.MockUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class LotsListActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private LotsAdapter adapter;

    public static void show(Context context) {
        context.startActivity(new Intent(context, LotsListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar.setTitle(R.string.slots_screen_title);

        setSupportActionBar(toolbar);
        setupAdapter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_slots;
    }

    private void setupAdapter() {
        adapter = new LotsAdapter(new LotsAdapter.OnLotClickListener() {
            @Override
            public void onLotClicked(LotItem item) {
                showLotDetails(item);
            }
        });
        adapter.setItems(MockUtils.getMockItems());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void showLotDetails(LotItem item) {
        LotDetailActivity.show(this);
    }
}
