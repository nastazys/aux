package com.vantage.app.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.vantage.app.R;
import com.vantage.app.ui.base.BaseActivity;

public class SignInActivity extends BaseActivity {

    public static void show(Context context) {
        context.startActivity(new Intent(context, SignInActivity.class));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_in;
    }
}
