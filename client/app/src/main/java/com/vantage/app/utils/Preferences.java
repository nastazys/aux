package com.vantage.app.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.vantage.app.App;

public class Preferences {

    private static final String TAG = Preferences.class.getSimpleName();
    public static final String IS_USER_AUTHORIZED_TAG = "IS_USER_AUTHORIZED_TAG";

    @Nullable
    private static String getStringPreferences(String key) {
        return getStringPreferences(key, null);
    }

    @Nullable
    private static String getStringPreferences(String key, @Nullable String defaultValue) {
        synchronized (key) {
            return getPreferences().getString(key, defaultValue);
        }
    }

    private static void setStringPreferences(String key, String value) {
        synchronized (key) {
            getPreferences().edit().putString(key, value).apply();
        }
    }

    private static boolean getBooleanPreferences(String key, boolean defaultValue) {
        synchronized (key) {
            return getPreferences().getBoolean(key, defaultValue);
        }
    }

    private static void setBooleanPreferences(String key, boolean value) {
        synchronized (key) {
            getPreferences().edit().putBoolean(key, value).apply();
        }
    }

    private static SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(App.getContext());
    }

    public static boolean isUserAuthorized(){
        // TODO: 2020-02-25  
        return true;
//        return getBooleanPreferences(IS_USER_AUTHORIZED_TAG, false);
    }
}
