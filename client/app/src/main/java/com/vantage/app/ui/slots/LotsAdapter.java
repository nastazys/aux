package com.vantage.app.ui.slots;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vantage.app.R;
import com.vantage.app.model.LotItem;

import java.util.ArrayList;

public class LotsAdapter extends RecyclerView.Adapter {

    private ArrayList<LotItem> items = new ArrayList<>();
    private OnLotClickListener listener;

    public interface OnLotClickListener{
        void onLotClicked(LotItem item);
    }

    public LotsAdapter(OnLotClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lot_view, parent, false);
        return new LotView(v, listener);    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((LotView)holder).bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(ArrayList<LotItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
